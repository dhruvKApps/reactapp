FROM node:14.17-alpine AS builder

WORKDIR /opt/web
#install make
RUN apk update && apk add make
#install bash
RUN apk add --no-cache bash

ARG targetFolder=public/

COPY $targetFolder/yarn.lock ./
COPY $targetFolder/package.json ./
COPY $targetFolder/packages/web/yarn.lock ./packages/web/
COPY $targetFolder/packages/web/package.json ./packages/web/
COPY $targetFolder/Makefile ./
COPY $targetFolder/lerna.json ./
RUN make install

##build web
COPY $targetFolder ./
RUN make build-web

#nulti stage docker build
FROM nginx:1.17-alpine
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-`cd -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin
COPY docker/nginx.web.conf /etc/nginx/nginx.template
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /opt/public/dist /usr/share/nginx/html
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]