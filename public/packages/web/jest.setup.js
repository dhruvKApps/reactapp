import { format } from 'util';
import 'jest-dom/extend-expect';

global.console = {
  // These are ignored in the tests
  warn: jest.fn(),
  debug: jest.fn(),
  info: jest.fn(),
  log: jest.fn(),

  // Keep native behaviour for other console methods
  error: console.error,
};

const error = global.console.error;
global.console.error = function (...args) {
  error(...args);
  throw new Error(format(...args));
};
