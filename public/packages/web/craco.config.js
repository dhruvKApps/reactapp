const { resolve } = require('path');
const sassResourcesLoader = require('craco-sass-resources-loader');

/**
 * Resolve tsconfig.json paths to Webpack aliases
 * @param  {string} jsconfigPath           - Path to tsconfig
 * @param  {string} basePath  - Path from jsconfig to Webpack config to create absolute aliases
 * @return {object}                        - Webpack alias config
 */
function resolveJsconfigPathsToWebpackAlias({
  tsconfigPath = './jsconfig.json',
  basePath = __dirname,
} = {}) {
  const { paths } = require(tsconfigPath).compilerOptions;

  const aliases = {};

  Object.keys(paths).forEach((item) => {
    const key = item.replace('/*', '');
    aliases[key] = resolve(
      basePath,
      paths[item][0].replace('/*', '').replace('*', '')
    );
  });

  return aliases;
}

/**
 * Converts paths defined in tsconfig.json to the format of
 * moduleNameMapper in jest.config.js.
 *
 * For example, {'@alias/*': [ 'path/to/alias/*' ]}
 * Becomes {'@alias/(.*)$': [ '<rootDir>/path/to/alias/$1' ]} OR
 *
 * {'@alias': [ 'alias' ]} if it is actually a file
 * Becomes {'@alias': [ '<rootDir>/path/to/alias' ]}
 * it works as long as the file type as long as the file type indicated in jest.moduleFileExtensions
 *
 * @param {string} tsconfigPath
 * @param {string} basePath
 * @return {object}                        - Jest moduleNameMapper
 */
function resolveJsconfigPathsToJestAlias({
  jsconfigPath = './jsconfig.json',
  basePath = __dirname,
} = {}) {
  // Get paths from tsconfig
  const { paths } = require(jsconfigPath).compilerOptions;
  console.log(paths);
  const aliases = {};

  // Iterate over paths and convert them into moduleNameMapper format
  Object.keys(paths).forEach((item) => {
    const key = `^${item.replace('/*', '/(.*)$')}`;
    const path = paths[item][0].replace('/*', '/$1'); // replace it only if it is a folder
    aliases[key] = path.replace(
      './',
      `<rootDir>/${basePath.replace('./', '')}/`
    );
  });
  return aliases;
}

module.exports = {
  reactScriptsVersion: 'react-scripts' /* (default value) */,
  plugins: [
    {
      plugin: {
        overrideWebpackConfig: ({ webpackConfig }) => {
          const gitHubIssueUrl = (repo, query) =>
            `https://github.com/${repo}/issues${
              query ? `?q=is%3Aissue+${query}` : ''
            }`;

          const throwInvalidConfigError = ({
            message,
            gitHubIssueQuery: query,
          }) => {
            throw new Error(
              `${message}\n\n` +
                'Did you update create-react-app or craco recently? \n' +
                'Please take a look at some recent issues in craco and ' +
                'create-react-app to see if someone has found a solution: \n\n' +
                `* ${gitHubIssueUrl('sharegate/craco', query)}\n` +
                `* ${gitHubIssueUrl('facebook/create-react-app', query)}\n`
            );
          };

          const oneOfRule = webpackConfig.module.rules.find(
            (rule) => typeof rule.oneOf !== 'undefined'
          );
          if (!oneOfRule) {
            throwInvalidConfigError({
              message:
                "Can't find a 'oneOf' rule under module.rules in the webpack config!",
              gitHubIssueQuery: 'webpack+rules+oneOf',
            });
          }
          return webpackConfig;
        },
      },
    },
    {
      plugin: sassResourcesLoader,
      options: {
        resources: './src/App.styles.scss',
      },
    },
  ],
  webpack: {
    alias: resolveJsconfigPathsToWebpackAlias({
      tsconfigPath: './jsconfig.json', // Using custom path
      basePath: './src',
    }),
  },
  jest: {
    configure: {
      // Runs this setup script before executing any of the tests
      setupFilesAfterEnv: '<rootDir>/jest.setup.js',
      // text & html are for human eyes, cobertura is for jenkins eyes
      testPathIgnorePatterns: ['<rootDir>/build/', '<rootDir>/node_modules/'],
      moduleNameMapper: resolveJsconfigPathsToJestAlias({
        tsconfigPath: './jsconfig.json', // Using custom path
        basePath: './src',
      }),
      roots: ['<rootDir>/src/'],
    },
  },
};
